# Pygame template - skeleton for a new pygame project
import pygame
import random
from os.path import join, dirname
from os import listdir
from engine.consts import *

class Game():
    def __init__(self, width, height, fps, caption, path):
        self.width = width
        self.height = height
        self.fps = fps
        self.caption = caption
        self.path = join(dirname(dirname(__file__)), path)
        self.img_path = join(self.path, "img")
        self.snd_path = join(self.path, "snd")
        self.font_name = pygame.font.match_font("arial")
        self.play_sounds = True
        
        # initialize pygame and create window
        pygame.init()
        pygame.mixer.init()
        self.screen = pygame.display.set_mode((self.width, self.height))
        pygame.display.set_caption(self.caption)
        self.clock = pygame.time.Clock()
        
        self.load_imgs()
        self.load_snds()
        self.init_game()

    def init_game(self):
        self.all_sprites = pygame.sprite.Group()
        self.add_sprites()

    def run(self):
        # Game loop
        self.running = True
        self.game_over = True
        
        while self.running:
            if self.game_over:
                self.show_gameover_screen()
                self.game_over = False
                self.init_game()
            
            self.clock.tick(self.fps)
            
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False
                self.check_event(event)

            self.all_sprites.update()
            self.update()

            self.draw_background()
            self.draw_sprites()
            self.draw_gui()
            
            pygame.display.flip()

        pygame.quit()

    def add_sprites(self):
        pass
        
    def update(self):
        pass
        
    def draw_background(self):
        self.screen.fill(BLACK)

    def draw_sprites(self):
        self.all_sprites.draw(self.screen)

    def draw_gui(self):
        pass
        
    def check_event(self, event):
        pass
        
    def load_imgs(self):
        self.imgs = {}
        #print("Loading images:")
        for img_name in listdir(self.img_path):
            #print(f" - {img_name}")
            img = pygame.image.load(join(self.img_path, img_name)).convert_alpha()
            self.imgs[img_name[:img_name.rfind(".")]] = img

    def load_snds(self):
        self.snds = {}
        #print("Loading sounds:")
        for snd_name in listdir(self.snd_path):
            #print(f" - {snd_name}")
            snd = pygame.mixer.Sound(join(self.snd_path, snd_name))
            snd.set_volume(0.5)
            self.snds[snd_name[:snd_name.rfind(".")]] = snd

    def draw_text(self, surf, text, size, x, y):
        font = pygame.font.Font(self.font_name, size)
        text_surface = font.render(text, True, WHITE)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x, y)
        surf.blit(text_surface, text_rect)
        
    def draw_bar(self, surf, x, y, color_fg, color_bg, percent):
        if percent < 0:
            percent = 0
        if percent > 1.0:
            percent = 1.0
        length = 100
        height = 10
        fill = int(round(length * percent))
        outline_rect = pygame.Rect(x, y, length, height)
        fill_rect = pygame.Rect(x, y, fill, height)
        pygame.draw.rect(surf, color_fg, fill_rect)
        pygame.draw.rect(surf, color_bg, outline_rect, 2)
        
    def show_gameover_screen(self):
        self.draw_background()