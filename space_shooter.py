import random
from os.path import join
import pygame
from engine.game import Game
from engine.consts import *
from space_shooter.player import Player
from space_shooter.mob import Mob, METEOR_SIZES
from space_shooter.explosion import Explosion
from space_shooter.powerup import Powerup

class SpaceShooter(Game):
    def __init__(self, width, height, fps, caption, path):
        super().__init__(width, height, fps, caption, path)
        self.score = 0
        pygame.mixer.music.load(join(self.path, "music", "music.oga"))
        pygame.mixer.music.set_volume(0.4)
        
        #pygame.mixer.music.play(loops=-1)
        self.play_sounds = False

    def add_sprites(self):
        super().add_sprites()
        self.score = 0
        self.bg_rect = self.imgs["starfield"].get_rect()
        
        self.player = Player(self)
        self.all_sprites.add(self.player)
        
        self.mobs = pygame.sprite.Group()
        self.create_mobs(8)

        self.bullets = pygame.sprite.Group()
        
        self.powerups = pygame.sprite.Group()
        
        self.explosion_anims = {}
        for size_nr, size in enumerate(METEOR_SIZES):
            self.explosion_anims[size] = []
            for nr in range(9):
                img = self.imgs[f'regularExplosion0{nr}']
                new_size = 15*(size_nr+1)
                new_img = pygame.transform.scale(img, (new_size, new_size))
                self.explosion_anims[size].append(new_img)
        self.explosion_anims['player'] = []        
        for nr in range(9):
            img = self.imgs[f'sonicExplosion0{nr}']
            self.explosion_anims['player'].append(img)
    
        self.imgs["player_mini"] = pygame.transform.scale(self.imgs["playerShip1_orange"], (25, 19))
    
    def update(self):
        super().update()
        
        # check to see if a bullet hit a mob
        hits = pygame.sprite.groupcollide(self.mobs, self.bullets, True, True)
        if hits:
            for hit in hits:
                self.score += 50 - hit.radius
                if self.play_sounds:
                    self.snds[f"expl{random.randint(1,2)}"].play()
                explosion = Explosion(self, hit.rect.center, hit.size)
                self.all_sprites.add(explosion)
                if random.random() > 0.9:
                    pow = Powerup(self, hit.rect.center)
                    self.all_sprites.add(pow)
                    self.powerups.add(pow)
            self.create_mobs(len(hits))
    
        if not self.player.hidden:
            # check to see if a mob hit the player
            hits = pygame.sprite.spritecollide(self.player, self.mobs, True, pygame.sprite.collide_circle)
            for hit in hits:
                self.create_mobs(1)
                explosion = Explosion(self, hit.rect.center, hit.size)
                self.all_sprites.add(explosion)
                self.player.shield -= hit.radius
                if self.player.shield <= 0:
                    self.death_explosion = Explosion(self, self.player.rect.center, "player")
                    self.all_sprites.add(self.death_explosion)
                    self.player.hide()
                    self.player.lives -= 1
                    self.player.shield += 100
                    if self.play_sounds:
                        self.snds["rumble1"].play()
                else:
                    if self.play_sounds:
                        self.snds[f"expl{random.randint(1,2)}"].play()
            
            # check to see if player hit a powerup
            hits = pygame.sprite.spritecollide(self.player, self.powerups, True)
            for hit in hits:
                if self.play_sounds:
                    self.snds[f"pow_{hit.type}"].play()

                if hit.type == "shield":
                    self.player.shield += random.randrange(10, 30)
                    if self.player.shield > 100:
                        self.player.shield = 100
                elif hit.type == "gun":
                    self.player.powerup()
            
        # if the player died and the explosion has finished playing
        if self.player.lives == 0 and not self.death_explosion.alive():
            self.game_over = True

    def draw_background(self):
        super().draw_background()
        self.screen.blit(self.imgs["starfield"], self.bg_rect)
        
    def draw_gui(self):
        super().draw_gui()
        self.draw_text(self.screen, str(self.score), 18, self.width / 2, 10)
        self.draw_bar(self.screen, 5, 5, GREEN, WHITE, self.player.shield/100.0)
        self.draw_lives(self.screen, self.width - 100, 5, self.player.lives, self.imgs["player_mini"])
    
    def draw_lives(self, surf, x, y, lives, img):
        for i in range(lives):
            img_rect = img.get_rect()
            img_rect.x = x + 30 * i
            img_rect.y = y
            surf.blit(img, img_rect)
        
    
    '''
    def check_event(self, event):
        super().check_event(event)
        if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    self.player.shoot()
    '''

    def create_mobs(self, amount):
        for i in range(amount):
            mob = Mob(self)
            self.all_sprites.add(mob)
            self.mobs.add(mob)
    
    def show_gameover_screen(self):
        super().show_gameover_screen()
        self.draw_text(self.screen, "SHMUP!", 64, self.width / 2, self.height / 4)
        self.draw_text(self.screen, "Arrow keys move, Space to fire", 22, self.width / 2, self.height / 2)
        self.draw_text(self.screen, "Press a key to begin", 18, self.width / 2, self.height * 0.75)
        pygame.display.flip()
        waiting = True
        while waiting:
            self.clock.tick(self.fps)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False
                elif event.type == pygame.KEYUP:
                    waiting = False
        
    
if __name__ == '__main__':
    game = SpaceShooter(800, 600, 60, "Shmup!", "space_shooter")
    game.run()