import random
import pygame
from engine.consts import * 

class Bullet(pygame.sprite.Sprite):
    def __init__(self, game, x, y):
        super().__init__()
        self.game = game
        #self.image = pygame.Surface((10, 20))
        #self.image.fill(YELLOW)
        self.image = self.game.imgs["laserRed16"]
        self.rect = self.image.get_rect()
        self.rect.centerx = x
        self.rect.bottom = y
        self.speedy = -10

    def update(self):
        self.rect.y += self.speedy
        # kill if it moves of the top of the screen
        if self.rect.bottom < 0:
            self.kill()
