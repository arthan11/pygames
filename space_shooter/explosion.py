import pygame

class Explosion(pygame.sprite.Sprite):
    def __init__(self, game, center, size):
        super().__init__()
        self.game = game
        self.size = size
        self.anim = self.game.explosion_anims[size]
        self.image = self.anim[0]
        self.rect = self.image.get_rect()
        self.rect.center = center
        self.frame = 0
        self.last_update = pygame.time.get_ticks()
        self.frame_rate = 25

    def update(self):
        new = pygame.time.get_ticks()
        if new - self.last_update > self.frame_rate:
            self.last_update = new
            self.frame += 1
            if self.frame >= len(self.anim):
                self.kill()
            else:
                center = self.rect.center
                self.image = self.anim[self.frame]
                self.rect = self.image.get_rect()
                self.rect.center = center