import random
import pygame
from engine.consts import * 

METEOR_SIZES = ["tiny", "small", "med", "big"]

class Mob(pygame.sprite.Sprite):
    def __init__(self, game):
        super().__init__()
        self.game = game
        #self.image = pygame.Surface((30, 40))
        #self.image.fill(RED)
        self.size = random.choice(METEOR_SIZES)
        self.image_orig = self.game.imgs[f"meteorBrown_{self.size}{random.randint(1, 2)}"]
        self.image = self.image_orig.copy()
        self.rect = self.image.get_rect()
        self.radius = int(self.rect.width * 0.40)
        #pygame.draw.circle(self.image, RED, self.rect.center, self.radius)
        self.random_params()
        self.rotation = 0
        self.rotation_speed = random.randrange(-8, 8)
        self.last_update = pygame.time.get_ticks()

    def is_on_screen(self):
        return (self.rect.top < self.game.height and
               self.rect.right > 0 and
               self.rect.left < self.game.width)
        
    def update(self):
        self.rotate()
        self.rect.y += self.speedy
        self.rect.x += self.speedx
        if not self.is_on_screen():
            self.random_params()
            
    def rotate(self):
        now = pygame.time.get_ticks()
        if now - self.last_update > 50:
            self.last_update = now
            old_center = self.rect.center
            self.rotation = (self.rotation + self.rotation_speed) % 360
            self.image = pygame.transform.rotate(self.image_orig, self.rotation)
            self.rect = self.image.get_rect()
            self.rect.center = old_center
        
    def random_params(self):
        self.rect.x = random.randrange(self.game.width - self.rect.width)
        self.rect.y = random.randrange(-150, -100)
        self.speedy = random.randrange(1, 8)
        self.speedx = random.randrange(-3, 3)
