import pygame
from engine.consts import *
from space_shooter.bullet import Bullet

class Player(pygame.sprite.Sprite):
    def __init__(self, game):
        super().__init__()
        self.game = game
        #self.image = pygame.Surface((50, 40))
        #self.image.fill(GREEN)
        self.image = pygame.transform.scale(self.game.imgs["playerShip1_orange"], (50, 38))
        self.rect = self.image.get_rect()
        self.radius = 20
        #pygame.draw.circle(self.image, RED, self.rect.center, self.radius)
        self.rect.centerx = self.game.width / 2
        self.rect.bottom = self.game.height - 10
        self.speedx = 8
        self.speedx_curr = 0
        self.shield = 100
        self.shoot_delay = 250
        self.last_shot = pygame.time.get_ticks()
        self.lives = 3
        self.hidden = False
        self.hide_timer = pygame.time.get_ticks()
        self.power = 1
        self.powerup_time = 5000
        self.power_timer = pygame.time.get_ticks()
        
    def update(self):
        # unhide if hidden
        if self.hidden and pygame.time.get_ticks() - self.hide_timer > 1000:
            self.hidden = False
            self.rect.centerx = self.game.width / 2
            self.rect.bottom = self.game.height - 10
        
        #timeup for powerups
        if self.power >= 2 and (pygame.time.get_ticks() - self.power_timer > self.powerup_time):
            self.power -= 1
            self.power_timer = pygame.time.get_ticks()
            
        
        if not self.hidden:
            self.speedx_curr = 0
            keystate = pygame.key.get_pressed()
            if keystate[pygame.K_LEFT]:
                self.speedx_curr -= self.speedx
            if keystate[pygame.K_RIGHT]:
                self.speedx_curr += self.speedx
            if keystate[pygame.K_SPACE]:
                self.shoot()
            self.rect.x += self.speedx_curr
            if self.rect.right > self.game.width:
                self.rect.right = self.game.width
            if self.rect.left < 0:
                self.rect.left = 0
                
    def shoot(self):
        now = pygame.time.get_ticks()
        if now - self.last_shot < self.shoot_delay:
            return
        self.last_shot = now
        if self.power == 1:
            bullet = Bullet(self.game, self.rect.centerx, self.rect.top)
            self.game.all_sprites.add(bullet)
            self.game.bullets.add(bullet)
        elif self.power == 2:
            bullet1 = Bullet(self.game, self.rect.left, self.rect.centery)
            bullet2 = Bullet(self.game, self.rect.right, self.rect.centery)
            self.game.all_sprites.add(bullet1)
            self.game.bullets.add(bullet1)
            self.game.all_sprites.add(bullet2)
            self.game.bullets.add(bullet2)
        elif self.power >= 3:
            bullet1 = Bullet(self.game, self.rect.left, self.rect.centery)
            bullet2 = Bullet(self.game, self.rect.right, self.rect.centery)
            bullet3 = Bullet(self.game, self.rect.centerx, self.rect.centery)
            self.game.all_sprites.add(bullet1)
            self.game.bullets.add(bullet1)
            self.game.all_sprites.add(bullet2)
            self.game.bullets.add(bullet2)
            self.game.all_sprites.add(bullet3)
            self.game.bullets.add(bullet3)

        if self.game.play_sounds:
            self.game.snds["pew"].play()
            
    def hide(self):
        self.hidden = True
        self.hide_timer = pygame.time.get_ticks()
        self.rect.center = (self.game.width / 2, self.game.height + 200)
        
    def powerup(self):
        self.power += 1
        self.power_timer = pygame.time.get_ticks()