import random
import pygame
from engine.consts import * 

POWERUP_TYPES = ["shield", "gun"]

class Powerup(pygame.sprite.Sprite):
    def __init__(self, game, center):
        super().__init__()
        self.game = game
        self.type = random.choice(POWERUP_TYPES)
        self.image = self.game.imgs[f"{self.type}_gold"]
        self.rect = self.image.get_rect()
        self.rect.center = center
        self.speedy = 2

    def update(self):
        self.rect.y += self.speedy
        # kill if it moves of the top of the screen
        if self.rect.top > self.game.height:
            self.kill()
